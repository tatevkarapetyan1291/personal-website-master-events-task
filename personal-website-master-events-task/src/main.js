document.addEventListener("DOMContentLoaded", function () {
  function run() {
    let section = document.createElement("section");
    let title = document.createElement("p");
    let subtitle = document.createElement("p");
    let form = document.createElement("form");
    let input = document.createElement("input");
    let button = document.createElement("button");

    section.classList.add("app-section__join-our-program","app-section__image")
    title.classList.add('title')
    subtitle.classList.add('subtitle')
    form.classList.add('input-area')

    title.innerText = 'Join Our Program'
    subtitle.innerHTML = 'Sed do eiusmod tempor incididunt </br> ut labore et dolore magna aliqua.'
    form.appendChild(input)
    form.appendChild(button)
    input.id = 'subscribe-value'
    button.id = 'subscribe'
    button.innerText = 'SUBSCRIBE'

    input.setAttribute("type",'text')
    input.setAttribute("placeholder",'Email')
    button.setAttribute("type",'submit')
    section.appendChild(title)
    section.appendChild(subtitle)
    section.appendChild(form)

   let learnmore =  document.getElementById('learn_more')
   learnmore.after(section);


   document.getElementById("subscribe").addEventListener("click", (event) => {
    event.preventDefault();
    const val = document.getElementById("subscribe-value").value;
    console.log(val);
  });
  }

  if (document.readyState != "loading") run();
  else if (document.addEventListener)
    document.addEventListener("DOMContentLoaded", run);
  else
    document.attachEvent("onreadystatechange", function () {
      if (document.readyState == "complete") run();
    });
});
